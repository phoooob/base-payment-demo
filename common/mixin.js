export default {
	data() {
		return {
			paidUrl : "https://gitee.com/phoooob/base-payment-demo/wikis/pages?sort_id=4111716&doc_id=1473806",
			configUrl : "https://gitee.com/phoooob/base-payment-demo/wikis/pages?sort_id=4111175&doc_id=1473806",
			uploadUrl : "https://gitee.com/phoooob/base-payment-demo/wikis/pages?sort_id=4139711&doc_id=1473806",
			transferUrl : "https://gitee.com/phoooob/base-payment-demo/wikis/pages?sort_id=4118025&doc_id=1473806",
			refundDoc : "https://gitee.com/phoooob/base-payment-demo/wikis/pages?sort_id=4111717&doc_id=1473806" ,
			queryRefundDoc : "https://gitee.com/phoooob/base-payment-demo/wikis/pages?sort_id=4111719&doc_id=1473806"
		}
	},
	methods : {
		copyUrl(url){
			// #ifndef H5
			uni.setClipboardData({
				data : url ,
				success: () => {
					uni.showToast({
						title:"链接已复制",
						icon:"success"
					})
				}
			})
			return ;
			// #endif
			window.open(url);
		},
	}
}