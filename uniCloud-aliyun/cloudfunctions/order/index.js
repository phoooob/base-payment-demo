/**
 * 创建商品订单，获取支付参数示例
 * 1. 该笔订单支付成功回调处理函数，需要在 base-payment > service > goodsService.js > paid() 中定义
 * 2. 若遇到  goodsService.js 文件加密无法打开的情况，请使用除HbuilderX之外的其他编辑器打开
 * 3. 此处只有getOrderInfo方法的调用是接口定义好的，其他的逻辑仅为示例作为参考，实际业务中请换成自己的逻辑。
 * 4. 获取支付参数参考文档：https://gitee.com/phoooob/base-payment-demo/wikis/pages?sort_id=4111715&doc_id=1473806
 * 5. 付款成功回调处理参考文档：https://gitee.com/phoooob/base-payment-demo/wikis/pages?sort_id=4111716&doc_id=1473806
 */
const {getOrderInfo} = require("base-pay"); //引入公共模块依赖base-pay
'use strict';
exports.main = async (event, context) => {
	//接收参数
	let {code , payType , type , goodsId , skuId , count } = event ;
	
	
	/**
	 * 获取支付参数前 自定义逻辑 start ==================================================
	 */
	
	//查询商品库存、价格等逻辑此处省略...
	let totalFee = 0.01 ; //商品价格
	let title = "碎花连衣裙" ; //商品标题
	let userId = "1" ; //当前用户ID
	
	//创建商品订单-示例，实际业务中请定义自己的业务逻辑
	await createCollection ("t_goods_order" , context); //腾讯云需要先创建数据表
	let goodsCollection = uniCloud.database().collection("t_goods_order") ;
	
	//自定义的商品订单表添加一条订单记录
	let {id} = await goodsCollection.add({
		goodsId ,
		skuId ,
		userId ,
		totalFee ,
		title ,
		status : 1
	})
	
	/**
	 * 获取支付参数前 自定义逻辑 end ==================================================
	 */
	
	
	
	//获取支付参数
	let res = await getOrderInfo({
		totalFee,
		title,
		code ,
		type, 
		payType,
		platform : context.PLATFORM ,
		//以下为其他任意需要保存的字段，可在支付成功通知中取用，如：
		userId,
		goodsId ,
		goodsOrderId : id 
	});
	
	
	
	/**
	 * 获取支付参数后 自定义逻辑 start ===============================================
	 */
	if (res.order) {
		//保存支付订单id到自定义的商品订单记录中
		await goodsCollection.doc(id).update({ payOrderId : res.order._id }) ;
	}
	/**
	 * 获取支付参数后 自定义逻辑 end==================================================
	 */
	
	
	//将支付参数原样返回给客户端
	return res ;
};

async function createCollection (name , context) {
	try{
		//腾讯云创建数据表
		if (context.SPACEINFO.provider == "tencent") {
			await uniCloud.database().createCollection(name);
		}
	}catch(e){
		
	}
}
