'use strict';
const { transfer } = require("base-pay");

/**
 * 微信转账接口示例
 * openid 为收款账号标识，通常在客户端申请转账时获取
 * 文档参考：https://gitee.com/phoooob/base-payment-demo/wikis/pages?sort_id=4118025&doc_id=1473806
 */
exports.main = async (event, context) => {
	let { platform , amount , title , realName  , openid } = event ;
	return await transfer({ 
		payType : "wxpay" , 
		platform , amount , title , realName , openid
	});
};
