'use strict';
const {queryOrder} = require("base-pay");

/**
 * 查询交易状态
 */
exports.main = async (event, context) => {
	return await queryOrder(event._id);
};
