'use strict';
const {
	transfer
} = require("base-pay");

/**
 * 支付宝转账接口示例
 */
exports.main = async (event, context) => {
	let { platform , amount , title , account , realName , remark } = event ;
	return await transfer({ 
		payType : "alipay" , 
		platform , amount , title , account , realName , remark
	});
};
