'use strict';
const {queryRefund} = require("base-pay");

/**
 * 退款查询接口示例
 * 必填参数：_id 退款订单号（本地数据库）
 */
exports.main = async (event, context) => {
	return await queryRefund(event._id);
};
