'use strict';
const {refund} = require("base-pay");

/**
 * 申请退款接口示例
 * 必填参数：_id , refundAmount , refundDesc
 * 可传入自定义参数，保存至退款记录数据中
 * 文档参考：https://gitee.com/phoooob/base-payment-demo/wikis/pages?sort_id=4111717&doc_id=1473806
 */
exports.main = async (event, context) => {
	
	//接收参数
	let {_id , refundAmount , refundDesc} = event ;
	
	//发起退款，退款后将在数据表base_payment_refund中创建一条退款记录，自定义的入库字段也将保存下来
	let res = await refund({
		_id , refundAmount , refundDesc ,
		goodsOrderId : 1006 //自定义入库字段
	});
	
	//返回退款结果
	return res ;
};
