import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false
App.mpType = 'app'

import mixin from "common/mixin.js" ;
Vue.mixin(mixin);

const app = new Vue({
    ...App
})
app.$mount()
