function setBasicAgg(agg, unwind, where, select, unselect, join , orderBy) {
	agg.unwind(unwind);
	agg.appendJoin(join);
	agg.where(where);
	agg.sort(orderBy);
	let start = Date.now();
	let alls = agg._combineSelect(select, unselect , join);
	agg.select(alls.select);
	agg.unselect(alls.unselect);
	return agg;
}

module.exports = async function paginate(args) {
	args = args || this.ctx.data;
	let {
		orderBy = "createTime desc",
		select,
		unselect,
		unwind,
		pageNumber = this.ctx.data.pageNumber || 1,
		pageSize = this.ctx.data.pageSize || 10,
		totalRow,
		join
	} = args;
	
	let where = this.getWhere(args);
console.log(JSON.parse(JSON.stringify(where)))
	let agg = this.agg();
	setBasicAgg(agg, unwind, where, select, unselect, join , orderBy);
	
	let list = await agg.getPage(pageNumber, pageSize);
	
	if (totalRow !== false) {
		let countAgg = this.agg();
		setBasicAgg(countAgg, unwind, where, select, unselect, join);
		totalRow = await countAgg.getCount();
	}
	
	return this.$b.getPage({
		pageNumber,
		pageSize,
		totalRow,
		list
	});
};
