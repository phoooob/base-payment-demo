const { Service } = require("base-cloud-v3");

module.exports = class GoodsService extends Service {

	/**
	 * 订单付款成功后
	 * @param {Object} order 统一支付订单信息
	 */
	async paid(order) {
		console.log("paid order :>> ", order);
		//更新商品订单的付款状态
		let goodsOrderId = order.goodsOrderId ;
		await uniCloud.database().collection("t_goods_order").doc(goodsOrderId).update({
			status : 2 //订单已付款
		});
	}
};